import core.DifficultyLevel;
import util.Engine;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        printSelectDifficultyMessage();
        DifficultyLevel difficulty = readDifficulty(scanner);
        Engine engine = new Engine(difficulty);
        engine.playMinesweeper(difficulty);
    }

    private static void printSelectDifficultyMessage() {
        System.out.println("Enter the Difficulty Level");
        System.out.println("BEGINNER (9 * 9 cells and 10 mines)");
        System.out.println("INTERMEDIATE (16 * 16 cells and 49 mines)");
        System.out.println("ADVANCED (24 * 24 cells and 99 mines)");
    }

    private static DifficultyLevel readDifficulty(Scanner scanner) {
        String input = scanner.nextLine();

        DifficultyLevel[] difficultyLevels = DifficultyLevel.values();
        for (DifficultyLevel difficultyLevel : difficultyLevels) {
            if(difficultyLevel.toString().equals(input.toUpperCase())) {
                return difficultyLevel;
            }
        }
        return DifficultyLevel.BEGINNER;
    }
}
