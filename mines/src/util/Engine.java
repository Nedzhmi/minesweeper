package util;

import core.Board;
import core.DifficultyLevel;

import java.util.Scanner;

public class Engine {

    private Scanner scanner;
    private Board realBoard;
    private int movesLeft;

    public Engine(DifficultyLevel difficultyLevel) {
        this.scanner = new Scanner(System.in);
        this.realBoard = new Board(difficultyLevel);
        this.movesLeft = this.realBoard.getMovesLeft();
    }

    public boolean playMinesweeperUtil(Board realBoard, Board myBoard, int row, int col) {
        if(myBoard.getCell(row, col) != '-') {
            return false;
        }

        if (realBoard.getCell(row, col) == '*') {
            myBoard.setCell(row, col, '*');

            myBoard.printBoard();
            System.out.println("You lost!");
            return true;
        } else {
            int count = realBoard.countAdjacentMines(row, col);
            this.movesLeft--;

            myBoard.setCell(row, col, (char) (count + '0'));

            if (count == 0) {
                if (myBoard.isValid(row - 1, col - 1)) {
                    playMinesweeperUtil(realBoard, myBoard, row - 1, col - 1);
                }
                if (myBoard.isValid(row - 1, col)) {
                    playMinesweeperUtil(realBoard, myBoard, row - 1, col);
                }
                if (myBoard.isValid(row - 1, col + 1)) {
                    playMinesweeperUtil(realBoard, myBoard, row - 1, col + 1);
                }
                if (myBoard.isValid(row, col - 1)) {
                    playMinesweeperUtil(realBoard, myBoard, row, col - 1);
                }
                if (myBoard.isValid(row, col + 1)) {
                    playMinesweeperUtil(realBoard, myBoard, row, col + 1);
                }
                if (myBoard.isValid(row + 1, col - 1)) {
                    playMinesweeperUtil(realBoard, myBoard, row + 1, col - 1);
                }
                if (myBoard.isValid(row + 1, col)) {
                    playMinesweeperUtil(realBoard, myBoard, row + 1, col);
                }
                if (myBoard.isValid(row + 1, col + 1)) {
                    playMinesweeperUtil(realBoard, myBoard, row + 1, col + 1);
                }
            }
            return false;
        }
    }

    public void playMinesweeper(DifficultyLevel difficultyLevel) {
        boolean gameOver = false;

        this.realBoard.placeMines();
        Board myBoard = new Board(difficultyLevel);

        int currMoveIndex = 0;
        while (!gameOver) {
            System.out.println("Current status of board my board:");
            myBoard.printBoard();
            System.out.println("Current status of board my real:");
            realBoard.printBoard();
            int[] moveIndexes = readMove(this.scanner);
            int rowIndex = moveIndexes[0];
            int colIndex = moveIndexes[1];

            if (currMoveIndex == 0) {
                if (this.realBoard.isMine(rowIndex, colIndex)) {
                    realBoard.replaceMine(rowIndex, colIndex);
                }
            }
            currMoveIndex++;

            gameOver = playMinesweeperUtil(realBoard, myBoard, rowIndex, colIndex);

            if (!gameOver && this.movesLeft == 0) {
                System.out.println("You have won!");
                gameOver = true;
            }
        }
    }

    private static int[] readMove(Scanner scanner) {
        int[] matrix = new int[2];
        System.out.print("Enter the move index for row\n->");
        matrix[0] = Integer.parseInt(scanner.nextLine());
        System.out.print("Enter the move index for col\n->");
        matrix[1] = Integer.parseInt(scanner.nextLine());
        return matrix;
    }
}
