package core;

import java.util.Random;

public class Board {

    private static final int BEGINNER_NUMBER_OF_MINES = 10;
    private static final int INTERMEDIATE_NUMBER_OF_MINES = 40;
    private static final int ADVANCED_NUMBER_OF_MINES = 99;
    private static final int BEGINNER_NUMBER_OF_ROWS = 9;
    private static final int INTERMEDIATE_NUMBER_OF_ROWS = 16;
    private static final int ADVANCED_NUMBER_OF_ROWS = 24;

    private DifficultyLevel difficultyLevel;
    private int rows;
    private int cols;
    private char[][] matrix;
    private int numOfMines;

    public Board(DifficultyLevel difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
        this.setRows();
        this.setCols();
        this.matrix = new char[rows][cols];
        this.initialise();
        this.setNumOfMines();
    }

    public char getCell(int row, int col) {
        if(isValid(row, col)) {
            return this.matrix[row][col];
        }
        return '*';
    }

    public int getMovesLeft() {
        return this.rows * this.cols - this.numOfMines;
    }

    public void setCell(int row, int col, char symbol) {
        this.matrix[row][col] = symbol;
    }

    public void setRows() {
        if (this.difficultyLevel == DifficultyLevel.ADVANCED) {
            this.rows = ADVANCED_NUMBER_OF_ROWS;
        } else if (this.difficultyLevel == DifficultyLevel.INTERMEDIATE) {
            this.rows = INTERMEDIATE_NUMBER_OF_ROWS;
        } else {
            this.rows = BEGINNER_NUMBER_OF_ROWS;
        }
    }

    public void setCols() {
        if (this.difficultyLevel == DifficultyLevel.ADVANCED) {
            this.cols = ADVANCED_NUMBER_OF_ROWS;
        } else if (this.difficultyLevel == DifficultyLevel.INTERMEDIATE) {
            this.cols = INTERMEDIATE_NUMBER_OF_ROWS;
        } else {
            this.cols = BEGINNER_NUMBER_OF_ROWS;
        }
    }

    public void setNumOfMines() {
        if (this.difficultyLevel == DifficultyLevel.ADVANCED) {
            this.numOfMines = ADVANCED_NUMBER_OF_MINES;
        } else if (this.difficultyLevel == DifficultyLevel.INTERMEDIATE) {
             this.numOfMines = INTERMEDIATE_NUMBER_OF_MINES;
        } else if (this.difficultyLevel == DifficultyLevel.BEGINNER){
            this.numOfMines = BEGINNER_NUMBER_OF_MINES;
        }
    }

    private void initialise() {
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                this.matrix[row][col] = '-';
            }
        }
    }
    
    public void printBoard() {
        for (int row = 0; row < this.matrix.length; row++) {
            for (int col = 0; col < this.matrix[row].length; col++) {
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }
    }

    public boolean isValid(int row, int col) {
        return row >= 0 && row < this.rows && col >= 0 && col < this.cols;
    }

    public boolean isMine(int row, int col) {
        return this.matrix[row][col] == '*';
    }

    public int countAdjacentMines(int row, int col) {
        int count = 0;

        for (int i = row - 1; i <= row + 1; i++) {
            for (int j = col - 1; j <= col + 1; j++) {
                if (i == row && j == col) {
                    continue;
                }
                if (isValid(i, j)) {
                    if (isMine(i, j)) {
                        count++;
                    }
                }
            }
        }

        return count;
    }

    public void placeMines() {
        int i = 0;
        while (i < numOfMines) {
            Random random = new Random();
            int rowIndex = random.nextInt(this.rows + 1);
            int colIndex = random.nextInt(this.cols + 1);

            if (isValid(rowIndex, colIndex)) {
                if (this.matrix[rowIndex][colIndex] != '*') {
                    this.matrix[rowIndex][colIndex] = '*';
                    i++;
                }
            }
        }
    }

    public void replaceMine(int row, int col) {
        for (int i = 0; i < this.rows; i++) {
            for (int j = 0; j < this.cols; j++) {
                if (this.matrix[i][j] != '*') {
                    this.matrix[i][j] = '*';
                    this.matrix[row][col] = '-';
                    return;
                }
            }
        }
    }
}
