package core;

public enum DifficultyLevel {
    BEGINNER, INTERMEDIATE, ADVANCED
}
